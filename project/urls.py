from django.conf import settings
from django.conf.urls import include, url
from django.contrib import admin

from welcome.views import index, health
from api.views import index, UserViewSet, BookViewSet, TranslationViewSet

urlpatterns = [
    # Examples:
    # url(r'^$', 'project.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    # url(r'^$', index),
    # url(r'^health$', health),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^users/', UserViewSet.as_view()),
    url(r'^books/', BookViewSet.as_view()),
    url(r'^translation/', TranslationViewSet.as_view()),
    url(r'^', index),
]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns
