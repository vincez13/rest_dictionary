# -*- coding: utf-8 -*-
# Generated by Django 1.11.14 on 2018-07-21 22:20
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0004_auto_20180722_0015'),
    ]

    operations = [
        migrations.AddField(
            model_name='translation',
            name='synonyms',
            field=models.CharField(default='', max_length=200),
        ),
    ]
