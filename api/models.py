from django.db import models


class Book(models.Model):
    title = models.CharField(max_length=200, default='', blank=True)
    isbn = models.CharField(max_length=20, default='', blank=True)

    def __str__(self):
        return self.title


class Translation(models.Model):
    en_word = models.CharField(max_length=100, default='')
    th_word = models.CharField(max_length=100, default='')
    identifier = models.CharField(max_length=20, default=None, blank=True)
    synonyms = models.CharField(max_length=200, default='', blank=True)
    add_date = models.DateTimeField('date added')
    relevant_books = models.ManyToManyField(Book, verbose_name='Books')

    def __str__(self):
        return self.en_word

