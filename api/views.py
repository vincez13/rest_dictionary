from django.http import HttpResponse
from rest_framework import serializers, viewsets, generics
from django.contrib.auth.models import User
from api.models import Book, Translation


def index(request):
    return HttpResponse("Hello")


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('username', 'email')


class UserViewSet(generics.ListAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class BookSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Book
        fields = ('title', 'isbn')


class TranslationSerializer(serializers.HyperlinkedModelSerializer):
    relevant_books = BookSerializer(many=True, read_only=True)

    class Meta:
        model = Translation
        fields = ('en_word', 'th_word', 'identifier', 'synonyms', 'add_date', 'relevant_books')


class BookViewSet(generics.ListAPIView):
    queryset = Book.objects.all()
    serializer_class = BookSerializer


class TranslationViewSet(generics.ListAPIView):
    queryset = Translation.objects.all()
    serializer_class = TranslationSerializer