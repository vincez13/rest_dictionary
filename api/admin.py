from django.contrib import admin
from api.models import Book, Translation

admin.site.register(Book)
admin.site.register(Translation)
